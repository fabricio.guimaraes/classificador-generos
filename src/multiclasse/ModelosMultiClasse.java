package multiclasse;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;

import com.google.gson.Gson;

import util.FileUtil;
import util.MensagemMestrado;
import util.PreprocessadorUtil;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.SMO;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.WekaPackageManager;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class ModelosMultiClasse {

	public static void main(String[] args)  throws Exception {
		
		Long dataInicio = System.currentTimeMillis();
		System.out.println("Inicio: " + new Date());
		WekaPackageManager.loadPackages(false, true, false);//		WekaPackageManager.loadPackages(false, true, false); 

		criarArquivoMultiRotulo();
		
//		criarModeloClassificador();
		
//		avaliarModeloMultiClasse();
//		avaliarModeloCascata();
//		avaliarFrameworkSinglelabel();
		
		Long tempoTotal = System.currentTimeMillis() - dataInicio;
		System.out.println("Fim: " + new Date());
		System.out.println("Tempo total: " + tempoTotal);		
	}
	
	private static void criarArquivoMultiRotulo() throws Exception {

		JSONParser jsonParser = new JSONParser();
		
		Set<String> mensagensRepetidas = new HashSet<String>();
		
//		int qteOneError = 0;
//		int qteHammingLoss = 0;
//		Double qteTotalDocumentos = 0.0;
		
		SMO smoFlat = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-flat-stwv.model");
		
		SMO smoAnuncio = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-anuncio-stwv.model");
		SMO smoDuvida = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-duvida-stwv.model");
		SMO smoEsclarecimento = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-esclarecimento-stwv.model");
		SMO smoInterpretacao = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-interpretacao-stwv.model");
		SMO smoOutros = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-SMO-outros-stwv.model");
		
		DataSource dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-flat-stwv_200.arff");
		Instances instancesFlatTeste = dataSourceFlatTeste.getDataSet();
		instancesFlatTeste.setClassIndex(0);
		
		DataSource dataSourceAnuncioTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-anuncio-stwv_200.arff");
		Instances instancesAnuncioTeste = dataSourceAnuncioTeste.getDataSet();
		instancesAnuncioTeste.setClassIndex(0);

		DataSource dataSourceDuvidaTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-duvida-stwv_200.arff");
		Instances instancesDuvidaTeste = dataSourceDuvidaTeste.getDataSet();
		instancesDuvidaTeste.setClassIndex(0);
		
		DataSource dataSourceEsclarecimentoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-esclarecimento-stwv_200.arff");
		Instances instancesEsclarecimentoTeste = dataSourceEsclarecimentoTeste.getDataSet();
		instancesEsclarecimentoTeste.setClassIndex(0);

		DataSource dataSourceInterpretacaoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-interpretacao-stwv_200.arff");
		Instances instancesInterpretacaoTeste = dataSourceInterpretacaoTeste.getDataSet();
		instancesInterpretacaoTeste.setClassIndex(0);
		
		DataSource dataSourceOutrosTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-outros-stwv_200.arff");
		Instances instancesOutrosTeste = dataSourceOutrosTeste.getDataSet();
		instancesOutrosTeste.setClassIndex(0);
		
		int i = 0; // de 0 a 902
		List<MensagemMestrado> mensagensMestrado = new ArrayList<MensagemMestrado>();
		List<String> mensagensMestradoJson = new ArrayList<String>();
		Gson gson = new Gson();
		
		try {
			String arquivoJson = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_902_json.json";
			JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(arquivoJson));
			Iterator<JSONObject> iterator = jsonArray.iterator();
			
			while (iterator.hasNext()) {
				
				/**
				 * Limpar mensagem
				 * */
				JSONObject jsonObject = iterator.next(); 
				String mensagemHtml = (String) jsonObject.get("message");
				String mensagem = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				if(mensagem == null || mensagem.isEmpty()) {
					continue;
				}
				
				mensagem = PreprocessadorUtil.limparMensagem(mensagem);
				
				if(!mensagensRepetidas.add(mensagem)) {
					continue;
				}

				if(mensagem.length() > 0 && mensagem.charAt(mensagem.length()-1) == '\\') {
					mensagem = mensagem.substring(0, mensagem.length()-1);
				}

				String genero = (String) jsonObject.get("genero");
				
				String mensagemLimpa = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				String classificador_hibrido = (String) jsonObject.get("classificador_hibrido");
				String classificador_flat = (String) jsonObject.get("classificador_flat");
				
				Long id_mensagem = (Long) jsonObject.get("id");
				
//				Long classificador_anuncio = (Long) jsonObject.get("classificador_anuncio");
//				Long classificador_duvida = (Long) jsonObject.get("classificador_duvida");
//				Long classificador_esclarecimento = (Long) jsonObject.get("classificador_esclarecimento");
//				Long classificador_interpretacao = (Long) jsonObject.get("classificador_interpretacao");
//				Long classificador_outros = (Long) jsonObject.get("classificador_outros");
				
				Long especialista_anuncio = (Long) jsonObject.get("especialista_anuncio");
				Long especialista_duvida = (Long) jsonObject.get("especialista_duvida");
				Long especialista_esclarecimento = (Long) jsonObject.get("especialista_esclarecimento");
				Long especialista_interpretacao = (Long) jsonObject.get("especialista_interpretacao");
				Long especialista_outros = (Long) jsonObject.get("especialista_outros");
			
				

				String classeCorreta = instancesFlatTeste.classAttribute().value((int) instancesFlatTeste.instance(i).classValue());
				
				double predFlat = smoFlat.classifyInstance(instancesFlatTeste.instance(i));
				String classeFlat = instancesFlatTeste.classAttribute().value((int) predFlat);
	
				double predAnuncio = smoAnuncio.classifyInstance(instancesAnuncioTeste.instance(i));
				String classeAnuncio = instancesAnuncioTeste.classAttribute().value((int) predAnuncio);
				
				double predDuvida = smoDuvida.classifyInstance(instancesDuvidaTeste.instance(i));
				String classeDuvida = instancesDuvidaTeste.classAttribute().value((int) predDuvida);
				
				double predEsclarecimento = smoEsclarecimento.classifyInstance(instancesEsclarecimentoTeste.instance(i));
				String classeEsclarecimento = instancesEsclarecimentoTeste.classAttribute().value((int) predEsclarecimento);
				
				double predInterpretacao = smoInterpretacao.classifyInstance(instancesInterpretacaoTeste.instance(i));
				String classeInterpretacao = instancesInterpretacaoTeste.classAttribute().value((int) predInterpretacao);
				
				double predOutros = smoOutros.classifyInstance(instancesOutrosTeste.instance(i));
				String classeOutros = instancesOutrosTeste.classAttribute().value((int) predOutros);
				
				System.out.print("ID: " + instancesFlatTeste.instance(i).value(0));
				System.out.print(", actual: " + classeCorreta);
				System.out.print(", Flat: " + classeFlat + " , anuncio: " + classeAnuncio + ", duvida: " + classeDuvida 
						+ ", esclarecimento: " + classeEsclarecimento + " , interpretacao: " + classeInterpretacao
						+ " , outros: " + classeOutros);
				System.out.println();
				
				MensagemMestrado mensagemMestrado = new MensagemMestrado();
				mensagemMestrado.setEspecialistaAnuncio(Integer.valueOf(especialista_anuncio.toString())); // setEspecialista_anuncio
				mensagemMestrado.setEspecialistaDuvida(Integer.valueOf(especialista_duvida.toString()));
				mensagemMestrado.setEspecialistaEsclarecimento(Integer.valueOf(especialista_esclarecimento.toString()));
				mensagemMestrado.setEspecialistaInterpretacao(Integer.valueOf(especialista_interpretacao.toString()));
				mensagemMestrado.setEspecialistaOutros(Integer.valueOf(especialista_outros.toString())); // setEspecialista_outros
				mensagemMestrado.setMessage(mensagemLimpa);
				mensagemMestrado.setUsuarioModificacao(-1); // setUsuario_modificacao
				mensagemMestrado.setClassificadorFlat(classificador_flat);
				mensagemMestrado.setClassificadorHibrido(classificador_hibrido);
				mensagemMestrado.setGenero(genero);
				
				mensagemMestrado.setClassificadorAnuncio(classeAnuncio.equalsIgnoreCase("A") ? 1 : 0); // setClassificador_anuncio
				mensagemMestrado.setClassificadorDuvida(classeDuvida.equalsIgnoreCase("D") ? 1 : 0);
				mensagemMestrado.setClassificadorEsclarecimento(classeEsclarecimento.equalsIgnoreCase("E") ? 1 : 0); // setClassificador_esclarecimento
				mensagemMestrado.setClassificadorInterpretacao(classeInterpretacao.equalsIgnoreCase("I") ? 1 : 0);
				mensagemMestrado.setClassificadorOutros(classeOutros.equalsIgnoreCase("O") ? 1 : 0);
				
				String mensagemMestradoJson = gson.toJson(mensagemMestrado);
				mensagensMestrado.add(mensagemMestrado);
				mensagensMestradoJson.add(mensagemMestradoJson);
				i++;
			}
//			System.out.println("iiiiiiiiiiiiiiiiiii-- " + i);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		String path = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_smo.json";
		FileUtil.salvarArquivo(mensagensMestradoJson.toString(), path, false); // mensagensMestrado.toString()
//		List<MensagemMestrado> mensagensMestrado = new ArrayList<MensagemMestrado>();
//		
//		double matrixConfusaoFlat [][] = new double [5][5];
//		double matrixConfusaoCascata [][] = new double[5][5];
//		
//		LibSVM smoFlat = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-flat-resample-stwv.model");
//		
//		LibSVM smoAnuncio = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-anuncio-resample-stwv.model");
//		LibSVM smoDuvida = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-duvida-resample-stwv.model");
//		LibSVM smoEsclarecimento = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-esclarecimento-resample-stwv.model");
//		LibSVM smoInterpretacao = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-interpretacao-resample-stwv.model");
//		LibSVM smoOutros = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-outros-resample-stwv.model");
//		
//		DataSource dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-flat-resample-stwv_200.arff");
//		Instances instancesFlatTeste = dataSourceFlatTeste.getDataSet();
//		instancesFlatTeste.setClassIndex(0);
//		
//		DataSource dataSourceAnuncioTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-anuncio-resample-stwv_200.arff");
//		Instances instancesAnuncioTeste = dataSourceAnuncioTeste.getDataSet();
//		instancesAnuncioTeste.setClassIndex(0);
//
//		DataSource dataSourceDuvidaTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-duvida-resample-stwv_200.arff");
//		Instances instancesDuvidaTeste = dataSourceDuvidaTeste.getDataSet();
//		instancesDuvidaTeste.setClassIndex(0);
//		
//		DataSource dataSourceEsclarecimentoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-esclarecimento-resample-stwv_200.arff");
//		Instances instancesEsclarecimentoTeste = dataSourceEsclarecimentoTeste.getDataSet();
//		instancesEsclarecimentoTeste.setClassIndex(0);
//
//		DataSource dataSourceInterpretacaoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-interpretacao-resample-stwv_200.arff");
//		Instances instancesInterpretacaoTeste = dataSourceInterpretacaoTeste.getDataSet();
//		instancesInterpretacaoTeste.setClassIndex(0);
//		
//		DataSource dataSourceOutrosTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-outros-resample-stwv_200.arff");
//		Instances instancesOutrosTeste = dataSourceOutrosTeste.getDataSet();
//		instancesOutrosTeste.setClassIndex(0);
//		
//		int totalAcertoFlat = 0;
//		int totalErroFlat = 0;
//		
//		int totalAcertoCascata = 0;
//		int totalErroCascata = 0;
//		
//		int totalAcertoCascataFlat = 0;
//		int totalErroCascataFlat = 0;
//		
//		int totalDuvidaCerto = 0;
//		int totalDuvidaErrado = 0;
//		
//		int totalAcertoHibrido = 0;
//		
//		for (int i = 0; i < instancesFlatTeste.numInstances(); i++) {
//			
//			String classeCorreta = instancesFlatTeste.classAttribute().value((int) instancesFlatTeste.instance(i).classValue());
//			
//			double predFlat = smoFlat.classifyInstance(instancesFlatTeste.instance(i));
//			String classeFlat = instancesFlatTeste.classAttribute().value((int) predFlat);
//
//			double predAnuncio = smoAnuncio.classifyInstance(instancesAnuncioTeste.instance(i));
//			String classeAnuncio = instancesAnuncioTeste.classAttribute().value((int) predAnuncio);
//			
//			double predDuvida = smoDuvida.classifyInstance(instancesDuvidaTeste.instance(i));
//			String classeDuvida = instancesDuvidaTeste.classAttribute().value((int) predDuvida);
//			
//			double predEsclarecimento = smoEsclarecimento.classifyInstance(instancesEsclarecimentoTeste.instance(i));
//			String classeEsclarecimento = instancesEsclarecimentoTeste.classAttribute().value((int) predEsclarecimento);
//			
//			double predInterpretacao = smoInterpretacao.classifyInstance(instancesInterpretacaoTeste.instance(i));
//			String classeInterpretacao = instancesInterpretacaoTeste.classAttribute().value((int) predInterpretacao);
//			
//			double predOutros = smoOutros.classifyInstance(instancesOutrosTeste.instance(i));
//			String classeOutros = instancesOutrosTeste.classAttribute().value((int) predOutros);
//			
//			System.out.print("ID: " + instancesFlatTeste.instance(i).value(0));
//			System.out.print(", actual: " + classeCorreta);
//			System.out.print(", Flat: " + classeFlat + " , anuncio: " + classeAnuncio + ", duvida: " + classeDuvida 
//					+ ", esclarecimento: " + classeEsclarecimento + " , interpretacao: " + classeInterpretacao
//					+ " , outros: " + classeOutros);
//			
//			if(classeFlat.equals(classeCorreta)) {
//				totalAcertoFlat++;
//			} else {
//				totalErroFlat++;
//			}
//			
//			criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoFlat);
//			
//			if(classeCorreta.equalsIgnoreCase(classeAnuncio) || classeCorreta.equalsIgnoreCase(classeDuvida) || 
//					classeCorreta.equalsIgnoreCase(classeEsclarecimento) || classeCorreta.equalsIgnoreCase(classeInterpretacao)) {
//				
//				totalAcertoHibrido++;
//			} else if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
//					&& classeInterpretacao.equalsIgnoreCase("N") && classeCorreta.equalsIgnoreCase(classeFlat)) {
//				totalAcertoHibrido++;
//			}
//			
//			if(classeAnuncio.equalsIgnoreCase("A") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
//					&& classeInterpretacao.equalsIgnoreCase("N")) {
//				System.out.println(" CASCATA: A "+ classeCorreta.equalsIgnoreCase("A"));
//				
//				if(classeCorreta.equalsIgnoreCase("A")) {
//					totalAcertoCascata++;
//				} else {
//					totalErroCascata++;
//				}
//
//				criarMatrixConfusao(classeCorreta, classeAnuncio, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("D") && classeEsclarecimento.equalsIgnoreCase("N") 
//					&& classeInterpretacao.equalsIgnoreCase("N")) {
//				System.out.println(" CASCATA: D "+ classeCorreta.equalsIgnoreCase("D"));
//				
//				if(classeCorreta.equalsIgnoreCase("D")) {
//					totalAcertoCascata++;
//				} else {
//					totalErroCascata++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeDuvida, matrixConfusaoCascata);
//				
//				continue;
//			}
//			
//			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("E") 
//					&& classeInterpretacao.equalsIgnoreCase("N")) {
//				System.out.println(" CASCATA: E "+ classeCorreta.equalsIgnoreCase("E"));
//				
//				if(classeCorreta.equalsIgnoreCase("E")) {
//					totalAcertoCascata++;
//				} else {
//					totalErroCascata++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeEsclarecimento, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
//					&& classeInterpretacao.equalsIgnoreCase("I")) {
//				System.out.println(" CASCATA: I "+ classeCorreta.equalsIgnoreCase("I"));
//				
//				if(classeCorreta.equalsIgnoreCase("I")) {
//					totalAcertoCascata++;
//				} else {
//					totalErroCascata++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeInterpretacao, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
//					&& classeInterpretacao.equalsIgnoreCase("N")) {
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata); //
////				criarMatrixConfusaoCascata(classeAtual, "O", matrixConfusaoCascata);
//				
//				if(classeCorreta.equalsIgnoreCase(classeFlat)) { //classeAtual.equalsIgnoreCase("O")
//					totalAcertoCascata++;
//				} else {
//					totalErroCascata++;
//				}
//				
//				System.out.println(" CASCATA: " + "N - flat " + classeFlat + "  acertou? " + classeCorreta.equalsIgnoreCase(classeFlat));
//				continue;
//			}
//
//			
//			if(classeFlat.equalsIgnoreCase(classeAnuncio)) {
//				System.out.println(" FLAT: A acertou? " + classeCorreta.equalsIgnoreCase("A"));
//				
//				if(classeCorreta.equalsIgnoreCase("A")) {
//					totalAcertoCascataFlat++;
//				} else {
//					totalErroCascataFlat++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeFlat.equalsIgnoreCase(classeDuvida)) {
//
//				System.out.println(" FLAT: D acertou? " + classeCorreta.equalsIgnoreCase("D"));
//				
//				if(classeCorreta.equalsIgnoreCase("D")) {
//					totalAcertoCascataFlat++;
//				} else {
//					totalErroCascataFlat++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeFlat.equalsIgnoreCase(classeEsclarecimento)) {
//				System.out.println(" FLAT: E acertou? " + classeCorreta.equalsIgnoreCase("E"));
//				
//				if(classeCorreta.equalsIgnoreCase("E")) {
//					totalAcertoCascataFlat++;
//				} else {
//					totalErroCascataFlat++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);				
//				continue;
//			}
//			
//			if(classeFlat.equalsIgnoreCase(classeInterpretacao)) {
//				System.out.println(" FLAT: I acertou? " + classeCorreta.equalsIgnoreCase("I"));
//				
//				if(classeCorreta.equalsIgnoreCase("I")) {
//					totalAcertoCascataFlat++;
//				} else {
//					totalErroCascataFlat++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeFlat.equalsIgnoreCase(classeOutros)) {
//				System.out.println(" FLAT: O acertou? " + classeCorreta.equalsIgnoreCase("O"));
//				
//				if(classeCorreta.equalsIgnoreCase("O")) {
//					totalAcertoCascataFlat++;
//				} else {
//					totalErroCascataFlat++;
//				}
//				
//				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
//				continue;
//			}
//			
//			if(classeCorreta.equalsIgnoreCase(classeFlat)) {
//				totalDuvidaCerto++;
//			} else {
//				totalDuvidaErrado++;
//			}
//			
//			criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
//			System.out.println(" CASCATA != FLAT (flat) : classeFlat acertou? " + classeCorreta.equalsIgnoreCase(classeFlat));
//			
//		}
//		
//		Integer qteTotalInstancias = instancesFlatTeste.numInstances();
//		System.out.println("Total Mensagens (instancias): " + qteTotalInstancias);
//		System.out.println();
//		System.out.println("FLAT");
//		double totalFlat = 0.0;
//		for(int i = 0; i < 5; i++) {
//			for(int j = 0; j < 5; j++) {
//				System.out.print(matrixConfusaoFlat[i][j] + " ");
//				totalFlat += matrixConfusaoFlat[i][j];
//			}
//			System.out.println();
//		}
//		System.out.println();
//		System.out.println("total flat: " + totalFlat);
//		double porcentagemAcertoFlat = (totalAcertoFlat / totalFlat) * 100;
//		System.out.println("total acerto: " + totalAcertoFlat + " % " + porcentagemAcertoFlat);
//		double porcentagemErroFlat = (totalErroFlat / totalFlat) * 100;
//		System.out.println("total ERRO: " + totalErroFlat + " % " + porcentagemErroFlat);
//		
//		System.out.println("framework");
//		System.out.println();
//		double totalCascata = 0.0;
//		for(int i = 0; i < 5; i++) {
//			for(int j = 0; j < 5; j++) {
//				System.out.print(matrixConfusaoCascata[i][j] + " ");
//				totalCascata += matrixConfusaoCascata[i][j];
//			}
//			System.out.println();
//		}
//		System.out.println();
//		System.out.println("total cascata: " + totalCascata);
//		System.out.println("total acerto cascata: " + totalAcertoCascata);
//		System.out.println("total ERRO cascata: " + totalErroCascata);
//		
//		System.out.println("total acerto cascata + flat: " + totalAcertoCascataFlat); 
//		System.out.println("total ERRO cascata + flat: " + totalErroCascataFlat);
//		
//		System.out.println("total cascata N -> usando o flat CERTO: " + totalDuvidaCerto);
//		System.out.println("total cascata N -> usando o flat ERRADO: " + totalDuvidaErrado);
//		
//		int totalAcertoCascataSomado = totalAcertoCascata + totalAcertoCascataFlat + totalDuvidaCerto;
//		double porcentagemAcertoCascata =  (totalAcertoCascataSomado / totalCascata) * 100;
//		
//		int totalErroCascataSomado = totalErroCascata + totalErroCascataFlat + totalDuvidaErrado;
//		double porcentagemErroCascata = (totalErroCascataSomado / totalCascata) * 100;
//		
//		System.out.println("total: " + totalAcertoCascataSomado + " % " + porcentagemAcertoCascata);
//		System.out.println("total: " + totalErroCascataSomado + " % " + porcentagemErroCascata);
//		
//		System.out.println("total acerto, se o cascata acertou em qualquer um, podendo ser multilabel: " + totalAcertoHibrido);
		
	}

	private static void avaliarModeloMultiClasse() throws Exception {
		
		double matrixConfusao [][] = new double [5][5];
		int totalAcertoFlat = 0;
		int totalErroFlat = 0;
		
		String algoritmo = "SMO"; // LIBSVM NAIVEBAYES SMO
		String classe = "flat"; // anuncio duvida esclarecimento interpretacao outros flat
		String filtro = "SMOTE"; // "" SMOTE resample
		String resultado = "";
		
		//SMO LibSVM NaiveBayes
		SMO smoFlat;
		DataSource dataSourceFlatTeste;
		
		if(filtro.isEmpty()) {
			smoFlat = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-" + algoritmo + "-" 
							+ classe + "-stwv.model");
			dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-" + classe + "-stwv_200.arff");
		} else {
			smoFlat = (SMO) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-" + algoritmo + "-" 
							+ classe + "-" + filtro + "-stwv.model");
			dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-" + classe + "-" + filtro 
					+ "-stwv_200.arff");
		}
		
		Instances instancesFlatTeste = dataSourceFlatTeste.getDataSet();
		instancesFlatTeste.setClassIndex(0);
		
		for (int i = 0; i < instancesFlatTeste.numInstances(); i++) {
			
			String classeCorreta = instancesFlatTeste.classAttribute().value((int) instancesFlatTeste.instance(i).classValue());
			
			double predFlat = smoFlat.classifyInstance(instancesFlatTeste.instance(i));
			String classeFlat = instancesFlatTeste.classAttribute().value((int) predFlat);
			
			if(classeFlat.equalsIgnoreCase(classeCorreta)) {
				totalAcertoFlat++;
			} else {
				totalErroFlat++;
			}
			
			criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusao);
		}
		
		System.out.println("Matrix Confusao");
		resultado += "Matrix Confusao\n";
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				System.out.print(matrixConfusao[i][j] + " ");
				resultado += matrixConfusao[i][j] + " ";
			}
			System.out.println();
			resultado += "\n";
		}
		double totalAcertoPorcentagem = (totalAcertoFlat / (double) instancesFlatTeste.size()) * 100;
		double totalErroPorcentagem = (totalErroFlat / (double) instancesFlatTeste.size()) * 100;
		
		System.out.println("Acuracia");
		resultado += "Acuracia\n";
		System.out.println("corretas: " + totalAcertoFlat + " % " + totalAcertoPorcentagem);
		resultado += "corretas: " + totalAcertoFlat + " % " + totalAcertoPorcentagem + "\n";
		System.out.println("incorretas: " + totalErroFlat + " % " + totalErroPorcentagem);
		resultado += "incorretas: " + totalErroFlat + " % " + totalErroPorcentagem + "\n";
		
		String caminhoSalvarArquivo;
		if(filtro.isEmpty()) {
			caminhoSalvarArquivo = "..//mestrado_weka//modelo_cascata//resultbuffer//teste1-" + algoritmo + "-"+ classe + "-stwv";
		} else {
			caminhoSalvarArquivo = "..//mestrado_weka//modelo_cascata//resultbuffer//teste1-" + algoritmo + "-"+ classe +"-" + filtro + "-stwv";
		}
		FileUtil.salvarArquivo(resultado, caminhoSalvarArquivo, false);
	}
	
	private static void avaliarModeloCascata() throws Exception {
		
		double matrixConfusao [][] = new double [5][5];
		
		String algoritmo = "SMO"; // LIBSVM NAIVEBAYES SMO
		String classe = "flat"; // anuncio duvida esclarecimento interpretacao outros flat
		String filtro = ""; // "" SMOTE resample
		String resultado = "";
		
		//SMO LibSVM NaiveBayes 
		NaiveBayes smoAnuncio = (NaiveBayes) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-NAIVEBAYES-anuncio-SMOTE-stwv.model");
		NaiveBayes smoDuvida = (NaiveBayes) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-NAIVEBAYES-duvida-SMOTE-stwv.model");
		NaiveBayes smoEsclarecimento = (NaiveBayes) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-NAIVEBAYES-esclarecimento-SMOTE-stwv.model");
		NaiveBayes smoInterpretacao = (NaiveBayes) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino1-NAIVEBAYES-interpretacao-SMOTE-stwv.model");
		
		DataSource dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-flat-SMOTE-stwv_200.arff");
		Instances instancesFlatTeste = dataSourceFlatTeste.getDataSet();
		instancesFlatTeste.setClassIndex(0);
		
		DataSource dataSourceAnuncioTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-anuncio-SMOTE-stwv_200.arff");
		Instances instancesAnuncioTeste = dataSourceAnuncioTeste.getDataSet();
		instancesAnuncioTeste.setClassIndex(0);

		DataSource dataSourceDuvidaTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-duvida-SMOTE-stwv_200.arff");
		Instances instancesDuvidaTeste = dataSourceDuvidaTeste.getDataSet();
		instancesDuvidaTeste.setClassIndex(0);
		
		DataSource dataSourceEsclarecimentoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-esclarecimento-SMOTE-stwv_200.arff");
		Instances instancesEsclarecimentoTeste = dataSourceEsclarecimentoTeste.getDataSet();
		instancesEsclarecimentoTeste.setClassIndex(0);

		DataSource dataSourceInterpretacaoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-interpretacao-SMOTE-stwv_200.arff");
		Instances instancesInterpretacaoTeste = dataSourceInterpretacaoTeste.getDataSet();
		instancesInterpretacaoTeste.setClassIndex(0);
		
		DataSource dataSourceOutrosTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste1-outros-SMOTE-stwv_200.arff");
		Instances instancesOutrosTeste = dataSourceOutrosTeste.getDataSet();
		instancesOutrosTeste.setClassIndex(0);
		
		int totalAcertoCascata = 0;
		int totalErroCascata = 0;
		
		for (int i = 0; i < instancesFlatTeste.numInstances(); i++) {
			
			String classeCorreta = instancesFlatTeste.classAttribute().value((int) instancesFlatTeste.instance(i).classValue());
			
			double predAnuncio = smoAnuncio.classifyInstance(instancesAnuncioTeste.instance(i));
			String classeAnuncio = instancesAnuncioTeste.classAttribute().value((int) predAnuncio);
			
			if(classeAnuncio.equals("A")) {
				if(classeAnuncio.equals(classeCorreta)) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				criarMatrixConfusao(classeCorreta, classeAnuncio, matrixConfusao);
				continue;
			}
			
			double predDuvida = smoDuvida.classifyInstance(instancesDuvidaTeste.instance(i));
			String classeDuvida = instancesDuvidaTeste.classAttribute().value((int) predDuvida);
			
			if(classeDuvida.equals("D")) {
				if(classeDuvida.equals(classeCorreta)) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				criarMatrixConfusao(classeCorreta, classeDuvida, matrixConfusao);
				continue;	
			}
			
			double predEsclarecimento = smoEsclarecimento.classifyInstance(instancesEsclarecimentoTeste.instance(i));
			String classeEsclarecimento = instancesEsclarecimentoTeste.classAttribute().value((int) predEsclarecimento);
			
			if(classeEsclarecimento.equals("E")) {
				if(classeEsclarecimento.equals(classeCorreta)) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				criarMatrixConfusao(classeCorreta, classeEsclarecimento, matrixConfusao);
				continue;
			}
			
			double predInterpretacao = smoInterpretacao.classifyInstance(instancesInterpretacaoTeste.instance(i));
			String classeInterpretacao = instancesInterpretacaoTeste.classAttribute().value((int) predInterpretacao);

			if(classeInterpretacao.equals("I")) {
				if(classeInterpretacao.equals(classeCorreta)) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				criarMatrixConfusao(classeCorreta, classeInterpretacao, matrixConfusao);
				continue;
			}
			
			if(classeCorreta.equals("O")) {
				totalAcertoCascata++;
			} else {
				totalErroCascata++;
			}
			criarMatrixConfusao(classeCorreta, "O", matrixConfusao);
			
		}
		
		System.out.println("Matrix Confusao");
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				System.out.print(matrixConfusao[i][j] + " ");
			}
			System.out.println();
		}
		double totalAcertoPorcentagem = (totalAcertoCascata / (double) instancesFlatTeste.size()) * 100;
		double totalErroPorcentagem = (totalErroCascata / (double) instancesFlatTeste.size()) * 100;
		
		System.out.println("corretas: " + totalAcertoCascata + " % " + totalAcertoPorcentagem);
		System.out.println("incorretas: " + totalErroCascata + " % " + totalErroPorcentagem);
	}
	
	private static void criarMatrixConfusao(String classeCorreta, String classePrevista, double matrixConfusao[][]) {
		
		if(classeCorreta.equalsIgnoreCase("A")) {
			if(classePrevista.equalsIgnoreCase(classeCorreta)) {
				matrixConfusao[0][0]++;
			} else {
				if(classePrevista.equalsIgnoreCase("D")) {
					matrixConfusao[0][1]++;
				}
				if(classePrevista.equalsIgnoreCase("E")) {
					matrixConfusao[0][2]++;
				}
				if(classePrevista.equalsIgnoreCase("I")) {
					matrixConfusao[0][3]++;
				}
				if(classePrevista.equalsIgnoreCase("O")) {
					matrixConfusao[0][4]++;
				}
			}
		}
		
		if(classeCorreta.equalsIgnoreCase("D")) {
			if(classePrevista.equalsIgnoreCase(classeCorreta)) {
				matrixConfusao[1][1]++;
			} else {
				if(classePrevista.equalsIgnoreCase("A")) {
					matrixConfusao[1][0]++;
				}
				if(classePrevista.equalsIgnoreCase("E")) {
					matrixConfusao[1][2]++;
				}
				if(classePrevista.equalsIgnoreCase("I")) {
					matrixConfusao[1][3]++;
				}
				if(classePrevista.equalsIgnoreCase("O")) {
					matrixConfusao[1][4]++;
				}
			}
		}
		
		if(classeCorreta.equalsIgnoreCase("E")) {
			if(classePrevista.equalsIgnoreCase(classeCorreta)) {
				matrixConfusao[2][2]++;
			} else {
				if(classePrevista.equalsIgnoreCase("A")) {
					matrixConfusao[2][0]++;
				}
				if(classePrevista.equalsIgnoreCase("D")) {
					matrixConfusao[2][1]++;
				}
				if(classePrevista.equalsIgnoreCase("I")) {
					matrixConfusao[2][3]++;
				}
				if(classePrevista.equalsIgnoreCase("O")) {
					matrixConfusao[2][4]++;
				}
			}
		}
		
		if(classeCorreta.equalsIgnoreCase("I")) {
			if(classePrevista.equalsIgnoreCase(classeCorreta)) {
				matrixConfusao[3][3]++;
			} else {
				if(classePrevista.equalsIgnoreCase("A")) {
					matrixConfusao[3][0]++;
				}
				if(classePrevista.equalsIgnoreCase("D")) {
					matrixConfusao[3][1]++;
				}
				if(classePrevista.equalsIgnoreCase("E")) {
					matrixConfusao[3][2]++;
				}
				if(classePrevista.equalsIgnoreCase("O")) {
					matrixConfusao[3][4]++;
				}
			}
		}
		
		if(classeCorreta.equalsIgnoreCase("O")) {
			if(classePrevista.equalsIgnoreCase(classeCorreta)) {
				matrixConfusao[4][4]++;
			} else {
				if(classePrevista.equalsIgnoreCase("A")) {
					matrixConfusao[4][0]++;
				}
				if(classePrevista.equalsIgnoreCase("D")) {
					matrixConfusao[4][1]++;
				}
				if(classePrevista.equalsIgnoreCase("E")) {
					matrixConfusao[4][2]++;
				}
				if(classePrevista.equalsIgnoreCase("I")) {
					matrixConfusao[4][3]++;
				}
			}
		}
	}
	
	private static void avaliarFrameworkSinglelabel() throws Exception {
		
		double matrixConfusaoFlat [][] = new double [5][5];
		double matrixConfusaoCascata [][] = new double[5][5];
		
		LibSVM smoFlat = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-flat-resample-stwv.model");
		
		LibSVM smoAnuncio = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-anuncio-resample-stwv.model");
		LibSVM smoDuvida = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-duvida-resample-stwv.model");
		LibSVM smoEsclarecimento = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-esclarecimento-resample-stwv.model");
		LibSVM smoInterpretacao = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-interpretacao-resample-stwv.model");
		LibSVM smoOutros = (LibSVM) SerializationHelper.read("..//mestrado_weka//modelo_cascata//models//treino-LIBSVM-outros-resample-stwv.model");
		
		DataSource dataSourceFlatTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-flat-resample-stwv_200.arff");
		Instances instancesFlatTeste = dataSourceFlatTeste.getDataSet();
		instancesFlatTeste.setClassIndex(0);
		
		DataSource dataSourceAnuncioTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-anuncio-resample-stwv_200.arff");
		Instances instancesAnuncioTeste = dataSourceAnuncioTeste.getDataSet();
		instancesAnuncioTeste.setClassIndex(0);

		DataSource dataSourceDuvidaTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-duvida-resample-stwv_200.arff");
		Instances instancesDuvidaTeste = dataSourceDuvidaTeste.getDataSet();
		instancesDuvidaTeste.setClassIndex(0);
		
		DataSource dataSourceEsclarecimentoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-esclarecimento-resample-stwv_200.arff");
		Instances instancesEsclarecimentoTeste = dataSourceEsclarecimentoTeste.getDataSet();
		instancesEsclarecimentoTeste.setClassIndex(0);

		DataSource dataSourceInterpretacaoTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-interpretacao-resample-stwv_200.arff");
		Instances instancesInterpretacaoTeste = dataSourceInterpretacaoTeste.getDataSet();
		instancesInterpretacaoTeste.setClassIndex(0);
		
		DataSource dataSourceOutrosTeste = new DataSource("..//mestrado_weka//modelo_cascata//preprocessado//teste-outros-resample-stwv_200.arff");
		Instances instancesOutrosTeste = dataSourceOutrosTeste.getDataSet();
		instancesOutrosTeste.setClassIndex(0);
		
		int totalAcertoFlat = 0;
		int totalErroFlat = 0;
		
		int totalAcertoCascata = 0;
		int totalErroCascata = 0;
		
		int totalAcertoCascataFlat = 0;
		int totalErroCascataFlat = 0;
		
		int totalDuvidaCerto = 0;
		int totalDuvidaErrado = 0;
		
		int totalAcertoHibrido = 0;
		
		for (int i = 0; i < instancesFlatTeste.numInstances(); i++) {
			
			String classeCorreta = instancesFlatTeste.classAttribute().value((int) instancesFlatTeste.instance(i).classValue());
			
			double predFlat = smoFlat.classifyInstance(instancesFlatTeste.instance(i));
			String classeFlat = instancesFlatTeste.classAttribute().value((int) predFlat);

			double predAnuncio = smoAnuncio.classifyInstance(instancesAnuncioTeste.instance(i));
			String classeAnuncio = instancesAnuncioTeste.classAttribute().value((int) predAnuncio);
			
			double predDuvida = smoDuvida.classifyInstance(instancesDuvidaTeste.instance(i));
			String classeDuvida = instancesDuvidaTeste.classAttribute().value((int) predDuvida);
			
			double predEsclarecimento = smoEsclarecimento.classifyInstance(instancesEsclarecimentoTeste.instance(i));
			String classeEsclarecimento = instancesEsclarecimentoTeste.classAttribute().value((int) predEsclarecimento);
			
			double predInterpretacao = smoInterpretacao.classifyInstance(instancesInterpretacaoTeste.instance(i));
			String classeInterpretacao = instancesInterpretacaoTeste.classAttribute().value((int) predInterpretacao);
			
			double predOutros = smoOutros.classifyInstance(instancesOutrosTeste.instance(i));
			String classeOutros = instancesOutrosTeste.classAttribute().value((int) predOutros);
			
			System.out.print("ID: " + instancesFlatTeste.instance(i).value(0));
			System.out.print(", actual: " + classeCorreta);
			System.out.print(", Flat: " + classeFlat + " , anuncio: " + classeAnuncio + ", duvida: " + classeDuvida 
					+ ", esclarecimento: " + classeEsclarecimento + " , interpretacao: " + classeInterpretacao
					+ " , outros: " + classeOutros);
			
			if(classeFlat.equals(classeCorreta)) {
				totalAcertoFlat++;
			} else {
				totalErroFlat++;
			}
			
			criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoFlat);
			
			if(classeCorreta.equalsIgnoreCase(classeAnuncio) || classeCorreta.equalsIgnoreCase(classeDuvida) || 
					classeCorreta.equalsIgnoreCase(classeEsclarecimento) || classeCorreta.equalsIgnoreCase(classeInterpretacao)) {
				
				totalAcertoHibrido++;
			} else if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
					&& classeInterpretacao.equalsIgnoreCase("N") && classeCorreta.equalsIgnoreCase(classeFlat)) {
				totalAcertoHibrido++;
			}
			
			if(classeAnuncio.equalsIgnoreCase("A") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
					&& classeInterpretacao.equalsIgnoreCase("N")) {
				System.out.println(" CASCATA: A "+ classeCorreta.equalsIgnoreCase("A"));
				
				if(classeCorreta.equalsIgnoreCase("A")) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}

				criarMatrixConfusao(classeCorreta, classeAnuncio, matrixConfusaoCascata);
				continue;
			}
			
			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("D") && classeEsclarecimento.equalsIgnoreCase("N") 
					&& classeInterpretacao.equalsIgnoreCase("N")) {
				System.out.println(" CASCATA: D "+ classeCorreta.equalsIgnoreCase("D"));
				
				if(classeCorreta.equalsIgnoreCase("D")) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				
				criarMatrixConfusao(classeCorreta, classeDuvida, matrixConfusaoCascata);
				
				continue;
			}
			
			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("E") 
					&& classeInterpretacao.equalsIgnoreCase("N")) {
				System.out.println(" CASCATA: E "+ classeCorreta.equalsIgnoreCase("E"));
				
				if(classeCorreta.equalsIgnoreCase("E")) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				
				criarMatrixConfusao(classeCorreta, classeEsclarecimento, matrixConfusaoCascata);
				continue;
			}
			
			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
					&& classeInterpretacao.equalsIgnoreCase("I")) {
				System.out.println(" CASCATA: I "+ classeCorreta.equalsIgnoreCase("I"));
				
				if(classeCorreta.equalsIgnoreCase("I")) {
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				
				criarMatrixConfusao(classeCorreta, classeInterpretacao, matrixConfusaoCascata);
				continue;
			}
			
			if(classeAnuncio.equalsIgnoreCase("N") && classeDuvida.equalsIgnoreCase("N") && classeEsclarecimento.equalsIgnoreCase("N") 
					&& classeInterpretacao.equalsIgnoreCase("N")) {
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata); //
//				criarMatrixConfusaoCascata(classeAtual, "O", matrixConfusaoCascata);
				
				if(classeCorreta.equalsIgnoreCase(classeFlat)) { //classeAtual.equalsIgnoreCase("O")
					totalAcertoCascata++;
				} else {
					totalErroCascata++;
				}
				
				System.out.println(" CASCATA: " + "N - flat " + classeFlat + "  acertou? " + classeCorreta.equalsIgnoreCase(classeFlat));
				continue;
			}

			
			if(classeFlat.equalsIgnoreCase(classeAnuncio)) {
				System.out.println(" FLAT: A acertou? " + classeCorreta.equalsIgnoreCase("A"));
				
				if(classeCorreta.equalsIgnoreCase("A")) {
					totalAcertoCascataFlat++;
				} else {
					totalErroCascataFlat++;
				}
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
				continue;
			}
			
			if(classeFlat.equalsIgnoreCase(classeDuvida)) {

				System.out.println(" FLAT: D acertou? " + classeCorreta.equalsIgnoreCase("D"));
				
				if(classeCorreta.equalsIgnoreCase("D")) {
					totalAcertoCascataFlat++;
				} else {
					totalErroCascataFlat++;
				}
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
				continue;
			}
			
			if(classeFlat.equalsIgnoreCase(classeEsclarecimento)) {
				System.out.println(" FLAT: E acertou? " + classeCorreta.equalsIgnoreCase("E"));
				
				if(classeCorreta.equalsIgnoreCase("E")) {
					totalAcertoCascataFlat++;
				} else {
					totalErroCascataFlat++;
				}
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);				
				continue;
			}
			
			if(classeFlat.equalsIgnoreCase(classeInterpretacao)) {
				System.out.println(" FLAT: I acertou? " + classeCorreta.equalsIgnoreCase("I"));
				
				if(classeCorreta.equalsIgnoreCase("I")) {
					totalAcertoCascataFlat++;
				} else {
					totalErroCascataFlat++;
				}
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
				continue;
			}
			
			if(classeFlat.equalsIgnoreCase(classeOutros)) {
				System.out.println(" FLAT: O acertou? " + classeCorreta.equalsIgnoreCase("O"));
				
				if(classeCorreta.equalsIgnoreCase("O")) {
					totalAcertoCascataFlat++;
				} else {
					totalErroCascataFlat++;
				}
				
				criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
				continue;
			}
			
			if(classeCorreta.equalsIgnoreCase(classeFlat)) {
				totalDuvidaCerto++;
			} else {
				totalDuvidaErrado++;
			}
			
			criarMatrixConfusao(classeCorreta, classeFlat, matrixConfusaoCascata);
			System.out.println(" CASCATA != FLAT (flat) : classeFlat acertou? " + classeCorreta.equalsIgnoreCase(classeFlat));
			
		}
		
		Integer qteTotalInstancias = instancesFlatTeste.numInstances();
		System.out.println("Total Mensagens (instancias): " + qteTotalInstancias);
		System.out.println();
		System.out.println("FLAT");
		double totalFlat = 0.0;
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				System.out.print(matrixConfusaoFlat[i][j] + " ");
				totalFlat += matrixConfusaoFlat[i][j];
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("total flat: " + totalFlat);
		double porcentagemAcertoFlat = (totalAcertoFlat / totalFlat) * 100;
		System.out.println("total acerto: " + totalAcertoFlat + " % " + porcentagemAcertoFlat);
		double porcentagemErroFlat = (totalErroFlat / totalFlat) * 100;
		System.out.println("total ERRO: " + totalErroFlat + " % " + porcentagemErroFlat);
		
		System.out.println("framework");
		System.out.println();
		double totalCascata = 0.0;
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				System.out.print(matrixConfusaoCascata[i][j] + " ");
				totalCascata += matrixConfusaoCascata[i][j];
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("total cascata: " + totalCascata);
		System.out.println("total acerto cascata: " + totalAcertoCascata);
		System.out.println("total ERRO cascata: " + totalErroCascata);
		
		System.out.println("total acerto cascata + flat: " + totalAcertoCascataFlat); 
		System.out.println("total ERRO cascata + flat: " + totalErroCascataFlat);
		
		System.out.println("total cascata N -> usando o flat CERTO: " + totalDuvidaCerto);
		System.out.println("total cascata N -> usando o flat ERRADO: " + totalDuvidaErrado);
		
		int totalAcertoCascataSomado = totalAcertoCascata + totalAcertoCascataFlat + totalDuvidaCerto;
		double porcentagemAcertoCascata =  (totalAcertoCascataSomado / totalCascata) * 100;
		
		int totalErroCascataSomado = totalErroCascata + totalErroCascataFlat + totalDuvidaErrado;
		double porcentagemErroCascata = (totalErroCascataSomado / totalCascata) * 100;
		
		System.out.println("total: " + totalAcertoCascataSomado + " % " + porcentagemAcertoCascata);
		System.out.println("total: " + totalErroCascataSomado + " % " + porcentagemErroCascata);
		
		System.out.println("total acerto, se o cascata acertou em qualquer um, podendo ser multilabel: " + totalAcertoHibrido);
	}
	
	private static void criarModeloClassificador() throws Exception {
		
	    WekaPackageManager.loadPackages(false, true, false); // Load all code in packages 
	    AbstractClassifier classifier = (AbstractClassifier)Class.forName("weka.classifiers.functions.LibSVM").newInstance(); 
		
	    String algoritmo = "NAIVEBAYES"; // LIBSVM NAIVEBAYES SMO
		String classe = "outros";
		String filtro = "SMOTE"; //"" SMOTE resample

		DataSource dataSourceTreino = new DataSource("..//mestrado_weka//modelo_cascata//dataset//treino_" + classe + ".arff");
		Instances instancesTreino = dataSourceTreino.getDataSet();
		instancesTreino.setClassIndex(1);
		
//		Resample resampleTreino = new Resample();
//		resampleTreino.setBiasToUniformClass(1.0);
//		resampleTreino.setInputFormat(instancesTreino);
//		instancesTreino = Filter.useFilter(instancesTreino, resampleTreino);
		
		SMOTE smoteTreino = new SMOTE();
		smoteTreino.setInputFormat(instancesTreino);
		instancesTreino = Filter.useFilter(instancesTreino, smoteTreino);
		
		StringToWordVector stringToWordVectorTreino = new StringToWordVector();
		stringToWordVectorTreino.setInputFormat(instancesTreino);
		stringToWordVectorTreino.setIDFTransform(true);
		stringToWordVectorTreino.setTFTransform(true);
		stringToWordVectorTreino.setAttributeIndices("first-last");
		instancesTreino = Filter.useFilter(instancesTreino, stringToWordVectorTreino);
		instancesTreino.setClassIndex(0);
		
		if(filtro.isEmpty()) {
			
			FileUtil.salvarArquivo(instancesTreino.toString(), "..//mestrado_weka//modelo_cascata//preprocessado//treino1-" 
					+ classe + "-stwv.arff", false);
		} else {
			FileUtil.salvarArquivo(instancesTreino.toString(), "..//mestrado_weka//modelo_cascata//preprocessado//treino1-" 
					+ classe + "-" + filtro + "-stwv.arff", false);
			
		}
		
//		LibSVM libSVM = (LibSVM) classifier;
//		SelectedTag s = new SelectedTag(0, LibSVM.TAGS_KERNELTYPE);
//		libSVM.setKernelType(s);
//		libSVM.buildClassifier(instancesTreino);
		
		NaiveBayes naivebayes = new NaiveBayes();
		naivebayes.buildClassifier(instancesTreino);
		
//		SMO smo = new SMO();
//		smo.buildClassifier(instancesTreino);
		
		if(filtro.isEmpty()){
			SerializationHelper.write("..//mestrado_weka//modelo_cascata//models//treino1-" + algoritmo + "-"+ classe + "-stwv.model", naivebayes);
		} else {
			SerializationHelper.write("..//mestrado_weka//modelo_cascata//models//treino1-" + algoritmo + "-"+ classe + "-" + filtro + "-stwv.model", naivebayes);
		}
		
//		String resultado = evaluation(libSVM, instancesTreino);
//		String caminhoSalvarArquivo = "..//mestrado_weka//modelo_cascata//resultbuffer//treino11-"+ classe +"-"+filtro+"-stwv";
//		FileUtil.salvarArquivo(resultado, caminhoSalvarArquivo, false);
		
		DataSource dataSourceTeste = new DataSource("..//mestrado_weka//modelo_cascata//dataset//teste_" + classe + "_200.arff");
		Instances instancesTeste = dataSourceTeste.getDataSet();
		instancesTeste.setClassIndex(1);
		instancesTeste = Filter.useFilter(instancesTeste, stringToWordVectorTreino);
		instancesTeste.setClassIndex(0);

		String caminho = "";
		
		if(filtro.isEmpty()) {
			caminho = "..//mestrado_weka//modelo_cascata//preprocessado//teste1-" + classe + "-stwv_200.arff";
		} else {
			caminho = "..//mestrado_weka//modelo_cascata//preprocessado//teste1-" + classe + 
					 "-" + filtro + "-stwv_200.arff";
		}
		FileUtil.salvarArquivo(instancesTeste.toString(), caminho, false);
	}
	

	public static String evaluation(Classifier classifier, Instances instances) throws Exception {

		StringBuilder resultBuffer = new StringBuilder();
		
		Evaluation evaluation = new Evaluation(instances);
		
		evaluation.crossValidateModel(classifier, instances, 10, new Random(10));

		String sumario = evaluation.toSummaryString(false);
		System.out.println(sumario);

		String matrixConfusao = evaluation.toMatrixString();
		System.out.println(matrixConfusao);

		String evaluationDetailed = evaluation.toClassDetailsString();
		System.out.println(evaluationDetailed);
		
		resultBuffer.append(sumario);
		resultBuffer.append("\n");
		resultBuffer.append(matrixConfusao);
		resultBuffer.append("\n");
		resultBuffer.append(evaluationDetailed);
		
		return resultBuffer.toString();
	}
}