package util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class FileUtil {
	
	public static void salvarArquivo(String content, String path, Boolean mesmoArquivo) {
		
		try {
			FileWriter writer = new FileWriter(new File(path), mesmoArquivo);
			BufferedWriter out = new BufferedWriter(writer); 
			out.write(content); 
			out.close(); 
		} catch (Exception e) { 
			e.printStackTrace(); 
		} 
	}

	public static BufferedReader lerArquivo(String path) {
		
		try {
			FileInputStream fstream = new FileInputStream(path);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			return br;

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			return null;
		}
	}
	
}