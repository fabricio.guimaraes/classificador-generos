package util;


public class MensagemMestrado { 

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private Integer ava;

	private Integer usuario;

	private Integer forumPost;

	private Integer polaridade;
	
	private String genero;

	private Long dataRegistro;
	
	private String message;

	private String classificadorFlat;
	
	private String classificadorHibrido;
	
	private Integer classificadorAnuncio = 0;
	
	private Integer classificadorDuvida = 0;
	
	private Integer classificadorEsclarecimento = 0;
	
	private Integer classificadorInterpretacao = 0;
	
	private Integer classificadorOutros = 0;
	
	private Integer especialistaAnuncio = 0;
	
	private Integer especialistaDuvida = 0;
	
	private Integer especialistaEsclarecimento = 0;
	
	private Integer especialistaInterpretacao = 0;
	
	private Integer especialistaOutros = 0;
	
	private Integer usuarioModificacao;
	
	private Long dataModificacao;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAva() {
		return ava;
	}

	public void setAva(Integer ava) {
		this.ava = ava;
	}

	public Integer getUsuario() {
		return usuario;
	}

	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}

	public Integer getForumPost() {
		return forumPost;
	}

	public void setForumPost(Integer forumPost) {
		this.forumPost = forumPost;
	}

	public Integer getPolaridade() {
		return polaridade;
	}

	public void setPolaridade(Integer polaridade) {
		this.polaridade = polaridade;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Long getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Long dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getClassificadorAnuncio() {
		return classificadorAnuncio;
	}

	public void setClassificadorAnuncio(Integer classificadorAnuncio) {
		this.classificadorAnuncio = classificadorAnuncio;
	}

	public Integer getClassificadorDuvida() {
		return classificadorDuvida;
	}

	public void setClassificadorDuvida(Integer classificadorDuvida) {
		this.classificadorDuvida = classificadorDuvida;
	}

	public Integer getClassificadorEsclarecimento() {
		return classificadorEsclarecimento;
	}

	public void setClassificadorEsclarecimento(Integer classificadorEsclarecimento) {
		this.classificadorEsclarecimento = classificadorEsclarecimento;
	}

	public Integer getClassificadorInterpretacao() {
		return classificadorInterpretacao;
	}

	public void setClassificadorInterpretacao(Integer classificadorInterpretacao) {
		this.classificadorInterpretacao = classificadorInterpretacao;
	}

	public Integer getClassificadorOutros() {
		return classificadorOutros;
	}

	public void setClassificadorOutros(Integer classificadorOutros) {
		this.classificadorOutros = classificadorOutros;
	}

	public Integer getEspecialistaAnuncio() {
		return especialistaAnuncio;
	}

	public void setEspecialistaAnuncio(Integer especialistaAnuncio) {
		this.especialistaAnuncio = especialistaAnuncio;
	}

	public Integer getEspecialistaDuvida() {
		return especialistaDuvida;
	}

	public void setEspecialistaDuvida(Integer especialistaDuvida) {
		this.especialistaDuvida = especialistaDuvida;
	}

	public Integer getEspecialistaEsclarecimento() {
		return especialistaEsclarecimento;
	}

	public void setEspecialistaEsclarecimento(Integer especialistaEsclarecimento) {
		this.especialistaEsclarecimento = especialistaEsclarecimento;
	}

	public Integer getEspecialistaInterpretacao() {
		return especialistaInterpretacao;
	}

	public void setEspecialistaInterpretacao(Integer especialistaInterpretacao) {
		this.especialistaInterpretacao = especialistaInterpretacao;
	}

	public Integer getEspecialistaOutros() {
		return especialistaOutros;
	}

	public void setEspecialistaOutros(Integer especialistaOutros) {
		this.especialistaOutros = especialistaOutros;
	}

	public Integer getUsuarioModificacao() {
		return usuarioModificacao;
	}

	public void setUsuarioModificacao(Integer usuarioModificacao) {
		this.usuarioModificacao = usuarioModificacao;
	}

	public Long getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Long dataModificacao) {
		this.dataModificacao = dataModificacao;
	}

	public String getClassificadorFlat() {
		return classificadorFlat;
	}

	public void setClassificadorFlat(String classificadorFlat) {
		this.classificadorFlat = classificadorFlat;
	}

	public String getClassificadorHibrido() {
		return classificadorHibrido;
	}

	public void setClassificadorHibrido(String classificadorHibrido) {
		this.classificadorHibrido = classificadorHibrido;
	}
}