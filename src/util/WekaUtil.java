package util;
import java.io.FileReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class WekaUtil {

public static void main(String[] args) throws Exception {
		
		Long dataInicio = System.currentTimeMillis();
		System.out.println("Inicio: " + new Date());
		
		String relation = "teste_flat_200";
		String atributo = "message";
		String atributoTipo = "string";
		String classe = "{A, D, E, I, O}"; //  {1,-1}; {A, D, E, I, O}; {A, N}}
		
		StringBuilder cabecalhoArff = WekaUtil.criarCabecalhoWeka(relation, atributo, atributoTipo, classe);

		String arquivoJson = "//home//fabricio//Workspace//mestrado_weka//modelo_cascata//dataset//teste_flat_200.json";
		
		WekaUtil.mensagemMestrado = new StringBuffer();
		String corpoArff = WekaUtil.corpoArffGenero(arquivoJson, "");
		
		StringBuilder arquivoArff = new StringBuilder();
		arquivoArff.append(cabecalhoArff);
		arquivoArff.append(corpoArff);
		String caminhoSalvarArquivo = "//home//fabricio//Workspace//mestrado_weka//modelo_cascata//dataset//novo-teste_flat_200.arff";
		FileUtil.salvarArquivo(arquivoArff.toString(), caminhoSalvarArquivo, false);

//		WekaUtil.mensagemMestrado = new StringBuffer();
		String caminhoSalvarMensagemMestrado = "//home//fabricio//Workspace//mestrado_weka//modelo_cascata//dataset//novo-mensagem-mestrado_200.arff";
		FileUtil.salvarArquivo(WekaUtil.mensagemMestrado.toString(), caminhoSalvarMensagemMestrado, false);
		
		Long tempoTotal = System.currentTimeMillis() - dataInicio;
		
		System.out.println("Fim: " + new Date());
		System.out.println("Tempo total: " + tempoTotal);
	}
	
	public static StringBuffer mensagemMestrado = new StringBuffer();
	
	public static String corpoArffGenero(String arquivoJson, String classeAtual) {
		
		JSONParser jsonParser = new JSONParser();
		
		Set<String> mensagensRepetidas = new HashSet<String>();
		StringBuilder corpoArff = new StringBuilder("@data\n");

		try {

			int contador = 0;
			JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(arquivoJson));
			Iterator<JSONObject> iterator = jsonArray.iterator();
			
			while (iterator.hasNext()) {
				
				/**
				 * Limpar mensagem
				 * */
				JSONObject jsonObject = iterator.next(); 
				String mensagemHtml = (String) jsonObject.get("message");
				String mensagem = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				if(mensagem == null || mensagem.isEmpty()) {
					continue;
				}
				
				mensagem = PreprocessadorUtil.limparMensagem(mensagem);
				
				if(!mensagensRepetidas.add(mensagem)) {
					continue;
				}

				if(mensagem.length() > 0 && mensagem.charAt(mensagem.length()-1) == '\\') {
					mensagem = mensagem.substring(0, mensagem.length()-1);
				}
				
				/**
				 * Tipar Mensagem
				 * */
				Object classe = jsonObject.get("genero");
				
				if(classe.equals("C")) {
					classe = "O";
				}
				
//				if(!classeAtual.isEmpty() && !classe.toString().isEmpty() && !classe.equals(classeAtual)) {
//					classe = "N";
//				}
				
				corpoArff.append("'" + mensagem + "'," + classe + ",");
				corpoArff.append("\n");
				
				Long id = (Long) jsonObject.get("id");
				Long id_ava = (Long) jsonObject.get("id_ava");
				Long id_usuario = (Long) jsonObject.get("id_usuario");
				Long id_forum_post = (Long) jsonObject.get("id_forum_post");
				Long polaridade = (Long) jsonObject.get("polaridade");
				String genero = (String) jsonObject.get("genero");
				Long data_registro_unixtime = (Long) jsonObject.get("data_registro_unixtime");
				
				String mensagemLimpa = PreprocessadorUtil.removeHtml(mensagemHtml);
				mensagemMestrado.append(id + "??" + id_forum_post + "??" + genero + "??" + mensagemLimpa + "??" + classe);
				mensagemMestrado.append("\n");
				
//				MensagemMestrado classificacaoMensagem = new MensagemMestrado();
//				
//				classificacaoMensagem.setAva(id_ava.intValue());
//				classificacaoMensagem.setDataRegistro(data_registro_unixtime);
//				classificacaoMensagem.setForumPost(id_forum_post.intValue());
//				classificacaoMensagem.setGenero(genero);
//				classificacaoMensagem.setId(id.intValue());
//				classificacaoMensagem.setPolaridade(polaridade.intValue());
//				classificacaoMensagem.setUsuario(id_usuario.intValue());
//				classificacaoMensagem.setMessage(mensagem);
//				
//				mensagemArr[contador] = classificacaoMensagem;
//				contador++;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return corpoArff.toString().substring(0, corpoArff.toString().length() - 2);
	}
	
	public static String criarDadosJsonArff(StringBuilder cabecalho, String arquivoJson, String campoAtributo, String campoClasse) {
		
		JSONParser jsonParser = new JSONParser();
		
		Set<String> mensagensRepetidas = new HashSet<String>();
		
		try {

			JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(arquivoJson));
			Iterator<JSONObject> iterator = jsonArray.iterator();
			
			while (iterator.hasNext()) {
				
				/**
				 * Limpar mensagem
				 * */
				JSONObject jsonObject = iterator.next(); 
				String mensagemHtml = (String) jsonObject.get(campoAtributo);
				String mensagem = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				if(mensagem == null || mensagem.isEmpty()) {
					continue;
				}
				
				mensagem = PreprocessadorUtil.limparMensagem(mensagem);
				
				if(!mensagensRepetidas.add(mensagem)) {
					continue;
				}
				
				if(mensagem.length() > 0 && mensagem.charAt(mensagem.length()-1) == '\\') {
					mensagem = mensagem.substring(0, mensagem.length()-1);
				}
				
				/**
				 * Tipar Mensagem
				 * */
				Object classe = jsonObject.get(campoClasse);
				
				if(!classe.equals("A")) {
					classe = "N";
				}
				
				cabecalho.append("'" + mensagem + "'," + classe + ",");
				cabecalho.append("\n");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return cabecalho.toString().substring(0, cabecalho.toString().length() - 2);
	}
	
	public static StringBuilder criarCabecalhoWeka(String relation, String atributo, String atributoTipo, String classe) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("@relation ");
		sb.append(relation);
		sb.append("\n");
		sb.append("@attribute ");
		sb.append(atributo);
		sb.append(" ");
		sb.append(atributoTipo);
		sb.append("\n");
		sb.append("@attribute class ");
		sb.append(classe);
		sb.append("\n");
//		sb.append("@data");
//		sb.append("\n");
		
		return sb;
	}
	
}
