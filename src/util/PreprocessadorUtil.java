package util;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;

public class PreprocessadorUtil {
	
	public static String limparMensagem(String mensagem) {
		
		mensagem = mensagem.toLowerCase();
		mensagem = removeEspacosBrancos(mensagem);
		mensagem = removeNumeros(mensagem);
		mensagem = removePontuacao(mensagem);
		mensagem = removeAcentuacao(mensagem);
		mensagem = removeStopWords(mensagem);
		mensagem = removePalavrasTamanho(mensagem, 2);
		mensagem = removeEspacosBrancos(mensagem);
		
		return mensagem;
	}
	
	public static String removeHtml(String mensagemHtml) {
		
		String mensagem = Jsoup.parse(mensagemHtml).text();
		return Jsoup.parse(mensagem).text();
	}
	
	public static String removeEspacosBrancos(String palavra) {
		
		palavra = palavra.trim();
		palavra = palavra.replaceAll("\\s+", " ");
		return palavra;
	}
	
	public static String removePalavrasTamanho(String palavra, int tamanho) {
		
		StringBuilder retorno = null;
		
		if(palavra != null && !palavra.isEmpty()) {
			String tokens [] = palavra.split(" ");
			retorno = new StringBuilder();
			for(String token : tokens) {
				if(token.length() > tamanho) {
					retorno.append(token);
					retorno.append(" ");
				}
			}
			
			return retorno.toString().trim();
		}
		return "";
	}
	
	public static String removeStopWords(String palavra) {
		
		String arquivoStopWordMestrado = "//home//fabricio//Workspace//mestrado_weka//modelo_cascata//dataset//stopwords";
		BufferedReader stopWords = FileUtil.lerArquivo(arquivoStopWordMestrado);
		String strLine;
		String tokens [] = null;

		if(palavra != null && !palavra.isEmpty()) {
			tokens = palavra.split(" ");
		}
		
		try {
			while ((strLine = stopWords.readLine()) != null) {

				for(int i = 0; i < tokens.length; i++) {
					if(tokens[i].isEmpty() || tokens[i].equals(strLine)) {
						tokens[i] = "";
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		StringBuilder retornoLimpo = new StringBuilder();
		
		if(tokens != null) {
			
			for(String token : tokens) {
				token = token.trim();
				if(!token.isEmpty()) {
					retornoLimpo.append(token);
					retornoLimpo.append(" ");				
				}
			}
		}
		
		return retornoLimpo.toString();
	}
	
	public static String removeAcentuacao(String palavra) {
		
		palavra = palavra.replace('á', 'a');
		palavra = palavra.replace('â', 'a');
		palavra = palavra.replace('ã', 'a');
		palavra = palavra.replace('à', 'a');
		
		palavra = palavra.replace('ç', 'c');
		
		palavra = palavra.replace('é', 'e');
		palavra = palavra.replace('ê', 'e');
		palavra = palavra.replace('è', 'e');
		
		palavra = palavra.replace('í', 'i');
		
		palavra = palavra.replace('ó', 'o');
		palavra = palavra.replace('ô', 'o');
		palavra = palavra.replace('õ', 'o');
		palavra = palavra.replace('ö', 'o');
		
		palavra = palavra.replace('ú', 'u');
		palavra = palavra.replace('ü', 'u');
		
		return palavra;
	}
	
	public static String removePontuacao(String palavra) {
		
		palavra = palavra.replace('!', ' ');
		palavra = palavra.replace('?', ' ');
		palavra = palavra.replace('.', ' ');
		palavra = palavra.replace(':', ' ');
		palavra = palavra.replace(';', ' ');
		palavra = palavra.replace(',', ' ');
		palavra = palavra.replace('/', ' ');
		palavra = palavra.replace('\\', ' ');
		palavra = palavra.replace('\'', ' ');
		palavra = palavra.replace('\"', ' ');
		palavra = palavra.replace('-', ' ');
		palavra = palavra.replace('+', ' ');
		palavra = palavra.replace('=', ' ');
		palavra = palavra.replace('*', ' ');
		palavra = palavra.replace('%', ' ');
		palavra = palavra.replace('_', ' ');
		palavra = palavra.replace(')', ' ');
		palavra = palavra.replace('(', ' ');
		palavra = palavra.replace(']', ' ');
		palavra = palavra.replace('[', ' ');
		palavra = palavra.replace('{', ' ');
		palavra = palavra.replace('}', ' ');
		palavra = palavra.replace('<', ' ');
		palavra = palavra.replace('>', ' ');
		palavra = palavra.replace('&', ' ');
		palavra = palavra.replace('$', ' ');
		palavra = palavra.replace('#', ' ');
		palavra = palavra.replace('@', ' ');
		palavra = palavra.replace('\'', ' ');
		palavra = palavra.replace('\'', ' ');
		palavra = palavra.replace('|', ' ');
		palavra = palavra.replace('ª', ' ');
		palavra = palavra.replace('º', ' ');
		palavra = palavra.replace('°', ' ');
		palavra = palavra.replace('§', ' ');
		palavra = palavra.replace('“', ' ');
		palavra = palavra.replace('’', ' ');
		palavra = palavra.replace('‘', ' ');
		palavra = palavra.replace('”', ' ');
		palavra = palavra.replace('–', ' ');
		
		return palavra;
	}
	
	public static String removeNumeros(String palavra) {
		
		palavra = palavra.replace('0', ' ');
		palavra = palavra.replace('1', ' ');
		palavra = palavra.replace('2', ' ');
		palavra = palavra.replace('3', ' ');
		palavra = palavra.replace('4', ' ');
		palavra = palavra.replace('5', ' ');
		palavra = palavra.replace('6', ' ');
		palavra = palavra.replace('7', ' ');
		palavra = palavra.replace('8', ' ');
		palavra = palavra.replace('9', ' ');
		
		return palavra;
	}
	
	public static void criarStopWords() {
		
		String arquivoStopWord1 = "//home//fabricio//workspace//mestrado_weka//resources//stopwords//lista de stopwords Portugues.txt";
		String arquivoStopWord2 = "//home//fabricio//workspace//mestrado_weka//resources//stopwords//stopwords.txt";
		String arquivoStopWordMestrado = "//home//fabricio//workspace//mestrado_weka//resources//stopwords//stopwordsmestrado";
		
		List<String> stopWords = new ArrayList<String>();
		
		BufferedReader stopWordsBR = FileUtil.lerArquivo(arquivoStopWord1);
		BufferedReader stopWordsBR2 = FileUtil.lerArquivo(arquivoStopWord2);
		String strLine;
		
		try {
			while ((strLine = stopWordsBR.readLine()) != null) {
				
				strLine = PreprocessadorUtil.removeAcentuacao(strLine);
				
				if(!stopWords.contains(strLine)) {
					stopWords.add(strLine);
				}
			}
			
			while ((strLine = stopWordsBR2.readLine()) != null) {
				
				strLine = PreprocessadorUtil.removeAcentuacao(strLine);
				
				if(!stopWords.contains(strLine)) {
					stopWords.add(strLine);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		StringBuilder swStopWords = new StringBuilder();
		
		for(String sw : stopWords) {
			swStopWords.append(sw);
			swStopWords.append("\n");
		}
		
		System.out.println(swStopWords);
		
		FileUtil.salvarArquivo(swStopWords.toString(), arquivoStopWordMestrado, false);
	}
	
	public static void preprocessarArquivoRoberto() {
		
		String arquivoOriginal = "//home//fabricio//workspace//mestrado_weka//resources//dados//preprocessados//todosRoberto//mensagem_roberto_preprocessamento.arff";
		String caminhoArquivoRobertoLimpo = "//home//fabricio//workspace//mestrado_weka//resources//dados//preprocessados//todosRoberto//mensagem_roberto_limpo.arff";
		
		BufferedReader stopWordsBR = FileUtil.lerArquivo(arquivoOriginal);
		String strLine;
		StringBuilder arquivoLimpo = new StringBuilder();
		
		try {
			while ((strLine = stopWordsBR.readLine()) != null) {

				strLine = strLine.toLowerCase();
				
				System.out.print(strLine + " - ");
				
				strLine = PreprocessadorUtil.removeAcentuacao(strLine);
				strLine = PreprocessadorUtil.removeStopWords(strLine);

				System.out.print(strLine + "\n");
				
				arquivoLimpo.append(strLine);
				arquivoLimpo.append("\n");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		FileUtil.salvarArquivo(arquivoLimpo.toString(), caminhoArquivoRobertoLimpo, false);
	}

}
