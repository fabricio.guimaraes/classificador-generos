package multirotulo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import mulan.classifier.lazy.MLkNN;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.transformation.AdaBoostMH;
import mulan.classifier.transformation.LabelPowerset;
import mulan.data.InvalidDataFormatException;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import util.FileUtil;
import util.PreprocessadorUtil;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;


public class ModelosMultiRotulo {

	public static void main(String[] args) throws Exception {
		
		Long dataInicio = System.currentTimeMillis();
		System.out.println("Inicio: " + new Date());

//		String arquivoJson = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_902_json.json";
		
		String nomeArquivo = "smo_smote";
		String arquivoJson = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_"+nomeArquivo+".json";
		
		avaliarAbordagemMultiRotulo(arquivoJson, nomeArquivo);
//		criarArquivoStwvMultilabel(arquivoJson, nomeArquivo);
		avaliarAlgoritmosMultiRotulo(nomeArquivo);
		
		Long tempoTotal = System.currentTimeMillis() - dataInicio;
		
		System.out.println("Fim: " + new Date());
		System.out.println("Tempo total: " + tempoTotal);
	}
	
	/**
	 * 
	 * Cria arquivo multilabel para ser utilizado com a ferramenta Weka
	 * */
	public static void criarArquivoStwvMultilabel(String arquivoJson, String nomeArquivo) throws Exception {
		
		String caminhoArquivoTempClassesMultilabel = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//temp_mensagem_mestrado_multilabel";
		String caminhoArquivoTempCorpoArff = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//temp_mensagem_mestrado-stwv-arquivoCorpoArff.arff";
		String caminhoArquivoTempMensagemMestradoSingleLabel = "//home//fabricio//Workspace//mestrado_multilabel//arquivos//temp_mensagem_mestrado_single_label-stwv.arff";
		
		//arquivo corpo arff para ser processado pelo stwv // arquivo com multilabel em cada linha (instancia)
		String resultadoCorpoArffGenero[] = corpoArffGenero(arquivoJson); 
		String arquivoArff =  resultadoCorpoArffGenero[0];
		FileUtil.salvarArquivo(arquivoArff.toString(), "//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_"+nomeArquivo+".arff", false);
		String classesMultilabel = resultadoCorpoArffGenero[1];
		FileUtil.salvarArquivo(classesMultilabel, caminhoArquivoTempClassesMultilabel, false);
		
		DataSource dataSourceTreino = new DataSource("//home//fabricio//Workspace//mestrado_multilabel//arquivos//dataset//mensagem_mestrado_"+nomeArquivo+".arff");
		Instances instancesTreino = dataSourceTreino.getDataSet();
		instancesTreino.setClassIndex(1);

		StringToWordVector stringToWordVectorTreino = new StringToWordVector(500);
		stringToWordVectorTreino.setInputFormat(instancesTreino);
		stringToWordVectorTreino.setIDFTransform(true);
		stringToWordVectorTreino.setTFTransform(true);
		stringToWordVectorTreino.setAttributeIndices("first-last");
		instancesTreino = Filter.useFilter(instancesTreino, stringToWordVectorTreino);
		instancesTreino.setClassIndex(0);
		FileUtil.salvarArquivo(instancesTreino.toString(), caminhoArquivoTempMensagemMestradoSingleLabel, false);

		//cria arquivo com classes-multilabel
		BufferedReader brArquivoArff = FileUtil.lerArquivo(caminhoArquivoTempMensagemMestradoSingleLabel);
		BufferedReader brArquivoMultilabel = FileUtil.lerArquivo(caminhoArquivoTempClassesMultilabel);
		
		StringBuilder cabecalhoMultilabel = new StringBuilder();
		cabecalhoMultilabel.append("@relation 'arquivo_multi-label: -c 5'\n");
		cabecalhoMultilabel.append("@attribute A {0,1}\n");
		cabecalhoMultilabel.append("@attribute D {0,1}\n");
		cabecalhoMultilabel.append("@attribute E {0,1}\n");
		cabecalhoMultilabel.append("@attribute I {0,1}\n");
		cabecalhoMultilabel.append("@attribute O {0,1}\n");
		
		//limpar cabecalho
		String linhaCabecalho = null;
		while((linhaCabecalho = brArquivoArff.readLine()) != null) {
			
			if(linhaCabecalho.contains("@relation")) {
				continue;
			}
			if(linhaCabecalho.contains("class")) {
				continue;
			}
			if(linhaCabecalho.contains("@data")) {
				break;
			}
			
			cabecalhoMultilabel.append(linhaCabecalho);
			cabecalhoMultilabel.append("\n");
		}
		
		cabecalhoMultilabel.append("@data\n");
		
		//limpar corpo do arquivo
		String arquivoCorpoArff = "";
		String linhaArffCompleto = null;
		while((linhaArffCompleto = brArquivoArff.readLine()) != null) {
			
			if(linhaArffCompleto.contains("@relation")) {
				continue;
			}
			if(linhaArffCompleto.contains("class")) {
				continue;
			}
			if(linhaArffCompleto.contains("@data")) {
				continue;
			}
			if(linhaArffCompleto.contains("numeric")) {
				continue;
			}
			
			linhaArffCompleto = linhaArffCompleto.replaceAll("[0-9]\\.[0-9]+", "1"); // retirando o peso
			linhaArffCompleto = linhaArffCompleto.replace("0 D," , "");
			linhaArffCompleto = linhaArffCompleto.replace("0 E," , "");
			linhaArffCompleto = linhaArffCompleto.replace("0 I," , "");
			linhaArffCompleto = linhaArffCompleto.replace("0 O," , "");
			linhaArffCompleto = linhaArffCompleto.replace("0 O}" , "");
			
			arquivoCorpoArff += linhaArffCompleto;
			arquivoCorpoArff += "\n";
		}
		
		FileUtil.salvarArquivo(arquivoCorpoArff, caminhoArquivoTempCorpoArff, false);
		BufferedReader brArquivoCorpoArff = FileUtil.lerArquivo(caminhoArquivoTempCorpoArff);
		
		String resultado = cabecalhoMultilabel.toString() + "\n";
		String linhaArff = null;
		while((linhaArff = brArquivoCorpoArff.readLine()) != null) {
			
			String linhaMultilabel = brArquivoMultilabel.readLine();
			resultado += "{" + linhaMultilabel;
			String numeros [] = linhaArff.replace("{", "").replace("1,", "").replace("1}", "").split(",");

			for(String n : numeros) {
				
				String numerosLinha [] = n.split(" "); 
				
				for(String numero : numerosLinha) {
					
					if(!numero.isEmpty()) {
						
						int numeroTotal = Integer.valueOf(numero);
						numeroTotal = numeroTotal + 4;
						resultado += numeroTotal + " 1,";
					}
				}
				resultado = resultado.toString().substring(0, resultado.toString().length() - 1);
				resultado += "}\n";
			}
		}
		
		FileUtil.salvarArquivo(resultado, "//home//fabricio//Workspace//mestrado_multilabel//arquivos//mulan_mensagem_mestrado_"+nomeArquivo+".arff", false);
		
		File tempClassesMultilabel = new File(caminhoArquivoTempClassesMultilabel);
		tempClassesMultilabel.delete();
		
		File tempCorpoArff = new File(caminhoArquivoTempCorpoArff);
		tempCorpoArff.delete();
		
		File tempMensagemMestradoSingleLabel = new File(caminhoArquivoTempMensagemMestradoSingleLabel);
		tempMensagemMestradoSingleLabel.delete();
	}

	public static void avaliarAbordagemMultiRotulo(String arquivoJson, String nomeArquivo) {
		
		JSONParser jsonParser = new JSONParser();
		
		Set<String> mensagensRepetidas = new HashSet<String>();
		
		int qteOneError = 0;
		int qteHammingLoss = 0;
		Double qteTotalDocumentos = 0.0;
		
		try {

			JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(arquivoJson));
			Iterator<JSONObject> iterator = jsonArray.iterator();
			
			while (iterator.hasNext()) {
				
				/**
				 * Limpar mensagem
				 * */
				JSONObject jsonObject = iterator.next(); 
				String mensagemHtml = (String) jsonObject.get("message");
				String mensagem = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				if(mensagem == null || mensagem.isEmpty()) {
					continue;
				}
				
				mensagem = PreprocessadorUtil.limparMensagem(mensagem);
				
				if(!mensagensRepetidas.add(mensagem)) {
					continue;
				}

				if(mensagem.length() > 0 && mensagem.charAt(mensagem.length()-1) == '\\') {
					mensagem = mensagem.substring(0, mensagem.length()-1);
				}

//				Long id = (Long) jsonObject.get("id");
//				Long id_ava = (Long) jsonObject.get("id_ava");
//				Long id_forum_post = (Long) jsonObject.get("id_forum_post");
//				Long polaridade = (Long) jsonObject.get("polaridade");
//				Long data_registro_unixtime = (Long) jsonObject.get("data_registro_unixtime");
//				Long id_usuario = (Long) jsonObject.get("id_usuario");
				
				Long usuario_modificacao = (Long) jsonObject.get("usuario_modificacao");
				if(usuario_modificacao == null) {
					break;
				}
				
				String genero = (String) jsonObject.get("genero");
				
				String mensagemLimpa = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				String classificador_hibrido = (String) jsonObject.get("classificador_hibrido");
				String classificador_flat = (String) jsonObject.get("classificador_flat");
				
				Long classificador_anuncio = (Long) jsonObject.get("classificador_anuncio");
				Long classificador_duvida = (Long) jsonObject.get("classificador_duvida");
				Long classificador_esclarecimento = (Long) jsonObject.get("classificador_esclarecimento");
				Long classificador_interpretacao = (Long) jsonObject.get("classificador_interpretacao");
				Long classificador_outros = (Long) jsonObject.get("classificador_outros");
				
				Long especialista_anuncio = (Long) jsonObject.get("especialista_anuncio");
				Long especialista_duvida = (Long) jsonObject.get("especialista_duvida");
				Long especialista_esclarecimento = (Long) jsonObject.get("especialista_esclarecimento");
				Long especialista_interpretacao = (Long) jsonObject.get("especialista_interpretacao");
				Long especialista_outros = (Long) jsonObject.get("especialista_outros");
				
				if(hammingLoss(classificador_anuncio, especialista_anuncio)) {
					qteHammingLoss = qteHammingLoss + 1;
				}
				if(hammingLoss(classificador_duvida, especialista_duvida)) {
					qteHammingLoss = qteHammingLoss + 1;
				}
				if(hammingLoss(classificador_esclarecimento, especialista_esclarecimento)) {
					qteHammingLoss = qteHammingLoss + 1;
				}
				if(hammingLoss(classificador_interpretacao, especialista_interpretacao)) {
					qteHammingLoss = qteHammingLoss + 1;
				}
				if(hammingLoss(classificador_outros, especialista_outros)) {
					qteHammingLoss = qteHammingLoss + 1;
				}
				
				boolean oneError = true;

				if(genero.equals("A") && classificador_anuncio == 1) {
					oneError = false;
				}
				
				if(genero.equals("D") && classificador_duvida == 1) {
					oneError = false;
				}
				
				if(genero.equals("E") && classificador_esclarecimento == 1) {
					oneError = false;
				}
				
				if(genero.equals("I") && classificador_interpretacao == 1) {
					oneError = false;
				}
				
				if(genero.equals("O") && classificador_outros == 1) {
					oneError = false;
				}
				
				if(oneError) {
					qteOneError++;
				}
				
				qteTotalDocumentos++;
				
				StringBuilder retorno = new StringBuilder(mensagemLimpa + "\n");
				retorno.append("hamming loss " + qteHammingLoss + " | oneError: " + qteOneError + "\n");
				retorno.append("especialista " + genero + " | flat: " + classificador_flat + " | hibrido: " + classificador_hibrido + "\n");
				retorno.append("anuncio " + classificador_anuncio + " | " + especialista_anuncio + " | " + "");
				retorno.append(hammingLoss(classificador_anuncio, especialista_anuncio) + "\n");
				retorno.append("duvida " + classificador_duvida + " | " + especialista_duvida + " | " + "");
				retorno.append(hammingLoss(classificador_duvida, especialista_duvida) + "\n");
				retorno.append("esclarecimento " + classificador_esclarecimento + " | " + especialista_esclarecimento + " | " + "");
				retorno.append(hammingLoss(classificador_esclarecimento, especialista_esclarecimento) + "\n");
				retorno.append("interpretacao " + classificador_interpretacao + " | " + especialista_interpretacao + " | " + "");
				retorno.append(hammingLoss(classificador_interpretacao, especialista_interpretacao) + "\n");
				retorno.append("outros " + classificador_outros + " | " + especialista_outros + " | " + "");
				retorno.append(hammingLoss(classificador_outros, especialista_outros) + "\n");
				
				System.out.println(retorno);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		StringBuilder results = new StringBuilder("*****Abordagem proposta*****");
		System.out.println("*****Abordagem proposta*****");
//		System.out.println("qte total documentos: " + qteTotalDocumentos);
		
		Double totalRotulos = (double) (5 * qteTotalDocumentos);
//		System.out.println("qte total de rotulos: " + totalRotulos);
		
		Double hammingLossRate = (double) (qteHammingLoss / totalRotulos);
		Double oneErrorRate = (double) (qteOneError / qteTotalDocumentos);
		
		results.append("\nhammingLossRate: " + hammingLossRate);
		System.out.println("hammingLossRate: " + hammingLossRate);
		
		results.append("\noneError Rate: " + oneErrorRate);
		System.out.println("oneError Rate: " + oneErrorRate);
		
		results.append("\n*****Abordagem proposta*****\n");
		System.out.println("*****Abordagem proposta*****");
		
		String path = "/home/fabricio/Dropbox/mestrado/dissertacao/resultado_multirotulo_"+nomeArquivo; 
		FileUtil.salvarArquivo(results.toString(), path, false);
	}
	
	private static boolean hammingLoss(Long classificador, Long especialista) {
		return !classificador.equals(especialista);
	}
	
	public static String [] corpoArffGenero(String arquivoJson) {
		
		JSONParser jsonParser = new JSONParser();
		
		Set<String> mensagensRepetidas = new HashSet<String>();
		
		StringBuilder cabecalho = new StringBuilder();
		cabecalho.append("@relation arff-stwv\n");
		cabecalho.append("@attribute message string\n");
		cabecalho.append("@attribute class {A, D, E, I, O}\n");
		cabecalho.append("@data\n");
		
		StringBuilder corpoArff = new StringBuilder(cabecalho);
		StringBuilder classesMultilabel = new StringBuilder();
		
		try {

			JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(arquivoJson));
			Iterator<JSONObject> iterator = jsonArray.iterator();
			
			while (iterator.hasNext()) {
				
				JSONObject jsonObject = iterator.next(); 
				String mensagemHtml = (String) jsonObject.get("message");
				String mensagem = PreprocessadorUtil.removeHtml(mensagemHtml);
				
				if(mensagem == null || mensagem.isEmpty()) {
					continue;
				}
				
				mensagem = PreprocessadorUtil.limparMensagem(mensagem);
				
				if(!mensagensRepetidas.add(mensagem)) {
					continue;
				}

				if(mensagem.length() > 0 && mensagem.charAt(mensagem.length()-1) == '\\') {
					mensagem = mensagem.substring(0, mensagem.length()-1);
				}
				Object classe = jsonObject.get("genero");
				
				Long usuario_modificacao = (Long) jsonObject.get("usuario_modificacao");
				if(usuario_modificacao == null) {
					break;
				}
			
				corpoArff.append("'" + mensagem + "'," + classe + ",");
				corpoArff.append("\n");
				
				Long especialista_anuncio = (Long) jsonObject.get("especialista_anuncio");
				Long especialista_duvida = (Long) jsonObject.get("especialista_duvida");
				Long especialista_esclarecimento = (Long) jsonObject.get("especialista_esclarecimento");
				Long especialista_interpretacao = (Long) jsonObject.get("especialista_interpretacao");
				Long especialista_outros = (Long) jsonObject.get("especialista_outros");
				
				if(especialista_anuncio == 1) {
					classesMultilabel.append("0 1, ");
				}

				if(especialista_duvida == 1) {
					classesMultilabel.append("1 1, ");
				}
				
				if(especialista_esclarecimento == 1) {
					classesMultilabel.append("2 1, ");
				}
				
				if(especialista_interpretacao == 1) {
					classesMultilabel.append("3 1, ");
				}
				
				if(especialista_outros == 1) {
					classesMultilabel.append("4 1, ");
				}
				
				classesMultilabel.append("\n");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		String retorno [] = new String [2];
		retorno[0] = corpoArff.toString().substring(0, corpoArff.toString().length() - 2);
		retorno[1] = classesMultilabel.toString();
		return retorno;
	}
	
	/**
	 * Usando as bibliotecas do Meka
	 * o formato do arquivo eh diferente, 
	 * @param arff classes antes dos atributos
	 * @param necessario o xml com as classes
	 * */
	public static void avaliarAlgoritmosMultiRotulo(String nomeArquivo) throws InvalidDataFormatException {
		
		StringBuilder resultadoArquivo = new StringBuilder();
//		AdaBoostMH adaBoostMH = new AdaBoostMH();
		RAkEL rAkEL = new RAkEL(); //new LabelPowerset(new J48())
		MLkNN mLkNN = new MLkNN();
		
		String arffFilename = "/home/fabricio/Workspace/mestrado_multilabel/arquivos/mulan_mensagem_mestrado_"+nomeArquivo+".arff";//Utils.getOption("arff", args); // e.g. -arff emotions.arff
		String xmlFilename = "/home/fabricio/Workspace/mestrado_multilabel/arquivos/mulan_mensagem_mestrado.xml";//Utils.getOption("xml", args); // e.g. -xml emotions.xml
		MultiLabelInstances dataset = new MultiLabelInstances(arffFilename, xmlFilename);

		Evaluator eval = new Evaluator();
		MultipleEvaluation results;
		results = eval.crossValidate(mLkNN, dataset, 10);

		System.out.println("*****MLKNN*****");
		System.out.println(results);
		resultadoArquivo.append("*****MLKNN*****\n");
		resultadoArquivo.append(results.toString());
		
//		results = null;
		
//		results = eval.crossValidate(adaBoostMH, dataset, 10);
//		System.out.println(results);
		
		results = null;
		results = eval.crossValidate(rAkEL, dataset, 10);
		
		System.out.println("*****RAKEL*****");
		System.out.println(results);
		resultadoArquivo.append("*****RAKEL*****\n");
		resultadoArquivo.append(results.toString());
		
		String path = "/home/fabricio/Dropbox/mestrado/dissertacao/resultado_multirotulo_"+nomeArquivo; 
		FileUtil.salvarArquivo(resultadoArquivo.toString(), path, true);
	}
}